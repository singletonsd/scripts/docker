#!/usr/bin/env bash

# Web Page of BASH best practices https://kvz.io/blog/2013/11/21/bash-best-practices/
#Exit when a command fails.
set -o errexit
#Exit when script tries to use undeclared variables.
set -o nounset
#The exit status of the last command that threw a non-zero exit code is returned.
set -o pipefail

#Trace what gets executed. Useful for debugging.
#set -o xtrace

# Set magic variables for current file & dir
__dir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
__file="${__dir}/$(basename "${BASH_SOURCE[0]}")"
__base="$(basename "${__file}" .sh)"
__root="$(cd "$(dirname "${__dir}")" && pwd)"

echo "Script name: ${__base}"
echo "Executing at ${__root}"

if ! type "docker" &> /dev/null; then
    echo "Docker is not installed. Install it and then re launch"
    exit 1
fi

if ! type "awk" &> /dev/null; then
    echo "awk is not installed. Install it and then re launch"
    exit 1
fi

if ! type "curl" &> /dev/null; then
    echo "curl is not installed. Install it and then re launch"
    exit 1
fi

function usage(){
    echo -e "-h | --help: display help."
    echo -e "-p | --push: push images after building."
    echo -e "-x | --proxy: use proxy."
    echo -e "-b | --base-name: base name of images."
    echo -e "-c | --commit-sha: sha of commit to attach to image."
    echo -e "-o | --only: the name of the image to create image."
    echo -e "-f | --folder: the name of folder containg image files."
    echo -e "-t | --tag: tag of images."
}

function buildImage() {
    echo "Building image name ${DOCKER_BUILD_IMAGE_NAME}-${BASE_IMAGE_TAG}"
    DOCKER_CONFIG="${DOCKER_BUILD_ENV_PROXY}" docker build --rm -f "${DOCKER_BUILD_FILE}" -t \
        "${DOCKER_BUILD_BASE_NAME}/${DOCKER_BUILD_IMAGE_NAME}-${BASE_IMAGE_TAG}:${DOCKER_BUILD_TAG}" \
        --label "version=${DOCKER_BUILD_TAG}" \
        --label "vcs-ref=${DOCKER_BUILD_COMMIT_SHA}" \
        --label "build-date=${DATE}" \
        --build-arg BASE_IMAGE_TAG="${BASE_IMAGE_TAG}" .
    if [ "${DOCKER_BUILD_PUSH}" == "1" ]; then
        docker push "${DOCKER_BUILD_BASE_NAME}/${DOCKER_BUILD_IMAGE_NAME}-${BASE_IMAGE_TAG}:${DOCKER_BUILD_TAG}"
    fi
}

DOCKER_BUILD_COMMIT_SHA="none"
DOCKER_BUILD_IMAGES_FOLDER="docker-images"

DOCKER_BUILD_ONLY=0
DOCKER_BUILD_FLAG_PROXY=0
DOCKER_BUILD_ENV_PROXY=""
DOCKER_BUILD_PUSH=0
DOCKER_BUILD_TAGS_FILES="tags"

while [ "${1+x}" != "" ]; do
    PARAM=$(echo "$1" | awk -F= '{print $1}')
    VALUE=$(echo "$1" | awk -F= '{print $2}')
    case "$PARAM" in
        -h | --help)
            usage
            exit
            ;;
        -p | --push)
            DOCKER_BUILD_PUSH=1
            ;;
        -x | --proxy)
            DOCKER_BUILD_FLAG_PROXY=1
            ;;
        -f | --folder)
            DOCKER_BUILD_IMAGES_FOLDER=$VALUE
            ;;
        -b | --base-name)
            DOCKER_BUILD_BASE_NAME=$VALUE
            ;;
        -c | --commit-sha)
            DOCKER_BUILD_COMMIT_SHA=$VALUE
            ;;
        -o | --only)
            DOCKER_BUILD_ONLY=1
            DOCKER_BUILD_ONLY_NAME=$VALUE
            ;;
        -t | --tag)
            DOCKER_BUILD_TAG=$VALUE
            ;;
        *)
            echo "ERROR: unknown parameter \"$PARAM\""
            usage
            exit 1
            ;;
    esac
    shift
done

if [ -z "${DOCKER_BUILD_BASE_NAME+x}" ]; then
    echo "ERROR: DOCKER_BUILD_BASE_NAME not set by environment nor argument."
    usage
    exit 1
fi

if [ -z "${DOCKER_BUILD_TAG+x}" ]; then
    DOCKER_BUILD_TAG="latest"
fi

DOCKER_BUILD_TAG="${DOCKER_BUILD_TAG////-}"
DOCKER_BUILD_TAG="${DOCKER_BUILD_TAG/feature/f}"

echo "Tag after sanitazing ${DOCKER_BUILD_TAG}"

if [ "${DOCKER_BUILD_COMMIT_SHA}" == "none" ]; then
    if type "git" &> /dev/null; then
        if [ -d .git ]; then
            if git log >/dev/null; then
                DOCKER_BUILD_COMMIT_SHA=$(git rev-parse HEAD | cut -c 1-8)
            fi
        fi
    fi
fi

DATE="$(date -u +'%Y-%m-%dT%H:%M:%SZ')"

if [ "${DOCKER_BUILD_FLAG_PROXY}" == "1" ]; then
    echo "Running with proxy environment."
    DOCKER_BUILD_ENV_PROXY=".docker/.proxy"
fi

if [ -f "$DOCKER_BUILD_IMAGES_FOLDER/script.sh" ]; then
    echo "Found docker-script, executing..."
    eval "$DOCKER_BUILD_IMAGES_FOLDER/script.sh"
fi

if [ -d "$DOCKER_BUILD_IMAGES_FOLDER" ]; then
    if [ "${DOCKER_BUILD_ONLY}" == "1" ]; then
        DOCKER_BUILD_SUBFOLDER="${DOCKER_BUILD_IMAGES_FOLDER}/${DOCKER_BUILD_ONLY_NAME}"
        DOCKER_BUILD_FILE="${DOCKER_BUILD_SUBFOLDER}/Dockerfile"
            if [ -f "${DOCKER_BUILD_FILE}" ]; then
                DOCKER_BUILD_IMAGE_NAME=$(basename "${DOCKER_BUILD_SUBFOLDER}")
                buildImage
            else
                echo "Dockerfile not found."
            fi
    else
        for DOCKER_BUILD_SUBFOLDER in "${DOCKER_BUILD_IMAGES_FOLDER}"/*/
        do
            echo "Found subfolder ${DOCKER_BUILD_SUBFOLDER}"
            DOCKER_BUILD_FILE="${DOCKER_BUILD_SUBFOLDER}/Dockerfile"
            if [ -f "${DOCKER_BUILD_FILE}" ]; then
                DOCKER_BUILD_IMAGE_NAME=$(basename "${DOCKER_BUILD_SUBFOLDER}")
                input="${DOCKER_BUILD_SUBFOLDER}/${DOCKER_BUILD_TAGS_FILES}"
                while IFS= read -r BASE_IMAGE_TAG
                do
                    buildImage
                done < "$input"
            else
                echo "Dockerfile not found."
            fi
        done
    fi
else
    if [ -f "Dockerfile" ]; then
        if [ -z "${SRC_FOLDER+x}" ]; then
            SRC_FOLDER="src"
        else
            echo "SRC_FOLDER provided: ${SRC_FOLDER}."
        fi
        DOCKER_BUILD_IMAGE_NAME=""
        DOCKER_BUILD_FILE="Dockerfile"
    fi
fi
